package com.seakale.minaExample.tcp.server;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.mina.core.filterchain.DefaultIoFilterChainBuilder;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.SocketAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

public class TCPServer
{
    public static void main(String[] args)
    {
        // 创建一个非阻塞的Server端Socket，用NIO
        SocketAcceptor acceptor = new NioSocketAcceptor();
        
        // 创建接受数据的过滤器
        DefaultIoFilterChainBuilder chain = acceptor.getFilterChain();
        
        // 设定这个过滤器将一行一行（/r/n）的读取数据
        chain.addLast("myChain", new ProtocolCodecFilter(new TextLineCodecFactory()));
        
        // 设定服务器端的消息处理器:一个SamplMinaServerHandler对象
        acceptor.setHandler(new SamplMinaServerHandler());
        
        // 服务器端绑定的端口
        int bindPort = 8899;
        
        // 绑定端口，启动服务器
        try
        {
            acceptor.bind(new InetSocketAddress(bindPort));
        }
        catch (IOException e)
        {
            
            e.printStackTrace();
        }
        System.out.println("Mina Server is Listing on:=" + bindPort);
    }
}
