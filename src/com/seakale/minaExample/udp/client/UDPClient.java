package com.seakale.minaExample.udp.client;

import java.net.InetSocketAddress;

import org.apache.mina.core.filterchain.DefaultIoFilterChainBuilder;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioDatagramConnector;


public class UDPClient
{
    public static void main(String[] args)
    {
        NioDatagramConnector con = new NioDatagramConnector();
        DefaultIoFilterChainBuilder chain = con.getFilterChain();
        chain.addLast("", new ProtocolCodecFilter(new TextLineCodecFactory()));
        
        con.setHandler(new ClientHandler());
        
        ConnectFuture cf = con.connect(new InetSocketAddress("127.0.0.1", 8899));
        cf.getSession().write("abc");
        
        cf.awaitUninterruptibly();
        con.dispose();
    }
}
