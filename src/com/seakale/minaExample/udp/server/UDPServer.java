package com.seakale.minaExample.udp.server;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.mina.core.filterchain.DefaultIoFilterChainBuilder;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioDatagramAcceptor;

public class UDPServer
{
    public static void main(String[] args)
    {
        //
        NioDatagramAcceptor con = new NioDatagramAcceptor();
        
        //
        DefaultIoFilterChainBuilder chain = con.getFilterChain();
        chain.addLast("", new ProtocolCodecFilter(new TextLineCodecFactory()));
        
        //
        con.setHandler(new ServerHandler());
        
        //
        try
        {
            con.bind(new InetSocketAddress(8899));
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        con.dispose();
        
    }
}
